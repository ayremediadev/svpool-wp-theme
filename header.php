<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'wp-bootstrap-starter' ); ?></a>
    <?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
	<header id="masthead" class="sticky-top site-header <?php echo wp_bootstrap_starter_bg_class(); ?>" role="banner">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 col-sm-2">
                    <nav class="navbar navbar-expand-lg p-0">
                    
                        <button id="trigger-overlay" class="navbar-toggler" type="button">
                            <span class="navbar-toggler-icon"></span>
                        </button>

                        <?php
                        wp_nav_menu(array(
                        'theme_location'  => 'primary',
                        'container'       => 'div',
                        'container_id'    => 'main-nav',
                        'container_class' => 'collapse navbar-collapse',
                        'menu_id'         => false,
                        'menu_class'      => 'navbar-nav',
                        'depth'           => 3,
                        'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                        'walker'          => new wp_bootstrap_navwalker()
                        ));
                        ?>
                    </nav>
                </div>

                <div class="col-md-2 col-sm-8 d-flex justify-content-around">
                    <div class="navbar-brand">
                        <a href="<?php echo esc_url( home_url( '/' )); ?>">
                            <img src="<?php bloginfo('template_directory'); ?>/inc/assets/img/logo.svg" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                        </a>
                    </div>
                </div>

                <div class="col-md-5 col-sm-2">

                    <nav class="navbar navbar-expand-lg p-0">
                    <?php
                    wp_nav_menu(array(
                    'theme_location'  => 'secondary',
                    'container'       => 'div',
                    'container_id'    => 'user-nav',
                    'container_class' => 'collapse navbar-collapse justify-content-end',
                    'menu_id'         => false,
                    'menu_class'      => 'navbar-nav',
                    'depth'           => 3,
                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
                    'walker'          => new wp_bootstrap_navwalker()
                    ));
                    ?>
                    </nav>
                </div>
                       
            </div>
        </div>
    <div class="overlay overlay-scale">
        <button type="button" class="overlay-close"><i class="fas fa-times"></i></button>

        <div class="logo">
              <a href="<?php echo esc_url( home_url( '/' )); ?>"><img src="<?php bloginfo('template_directory'); ?>/inc/assets/img/logo-dark.svg" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>"></a>
         </div>

        

        <?php
        wp_nav_menu(array(
        'theme_location'  => 'primary',
        'container_id'    => 'main-nav-mobile',
        'menu_class'      => 'mobile-nav',
        'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
        'walker'          => new wp_bootstrap_navwalker()
        ));
        ?>

        <h3 class="text-center">Your Account</h3>
        
         <?php
        wp_nav_menu(array(
        'theme_location'  => 'secondary',
        'container_id'    => 'user-nav-mobile',
        'menu_class'      => 'mobile-nav',
        'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
        'walker'          => new wp_bootstrap_navwalker()
        ));
        ?>
    </div>
	</header><!-- #masthead -->
    <?php if(is_front_page() && !get_theme_mod( 'header_banner_visibility' )): ?>
        <div id="page-sub-header" <?php if(has_header_image()) { ?>style="background-image: url('<?php header_image(); ?>');" <?php } ?>>
            <div class="container">
                <h1>
                    <?php
                    if(get_theme_mod( 'header_banner_title_setting' )){
                        echo get_theme_mod( 'header_banner_title_setting' );
                    }else{
                        echo 'Wordpress + Bootstrap';
                    }
                    ?>
                </h1>
                <p>
                    <?php
                    if(get_theme_mod( 'header_banner_tagline_setting' )){
                        echo get_theme_mod( 'header_banner_tagline_setting' );
                }else{
                        echo esc_html__('To customize the contents of this header banner and other elements of your site, go to Dashboard > Appearance > Customize','wp-bootstrap-starter');
                    }
                    ?>
                </p>
                <a href="#content" class="page-scroller"><i class="fa fa-fw fa-angle-down"></i></a>
            </div>
        </div>
    <?php endif; ?>
		
                <?php endif; ?>