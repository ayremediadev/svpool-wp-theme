<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<div id="content" class="site-content page">

	<div class="container-fluid">
		<div class="row">	

			<section id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

				<div class="entry-content error-404 not-found">
					<p><!-- wp:html --><br></p>
					<header class="entry-header">
                		<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'wp-bootstrap-starter' ); ?></h1>
           			 </header>
           			 <div class="header-border">
		                <div class="top-left"></div>
		                <div class="top-right"></div>
		                <div class="bottom-left"></div>
		                <div class="bottom-right"></div>
		            </div>

		            <div class="container col-md-8 top-skew" style="min-height: 200px">
						<p><?php esc_html_e( 'It looks like nothing was found at this location.', 'wp-bootstrap-starter' ); ?></p>
					</div>


				</div><!-- .error-404 -->

				</main><!-- #main -->
			</section><!-- #primary -->

		</div><!-- .row -->
	</div><!-- .container -->

</div><!-- #content -->
<?php

get_footer();
