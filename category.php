<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<div id="content" class="site-content page">
	<div class="container-fluid">
		<div class="row">

			<section id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

					<div class="entry-content">
						<p><!-- wp:html --><br></p>
						<header class="entry-header">
	                		<h1 class="page-title"><?php single_cat_title(); ?></h1>
	           			 </header>
	           			 <div class="header-border">
			                <div class="top-left"></div>
			                <div class="top-right"></div>
			                <div class="bottom-left"></div>
			                <div class="bottom-right"></div>
			            </div>
			            <p><!-- wp:html --><br></p>

			            <div class="page-content container col-8">
			            	<div class="listing row news-block">

								<?php
							        if ( have_posts() ) : ?>						          
							            <div class="card-deck">
							            <?php
							            /* Start the Loop */
							            while ( have_posts() ) : the_post();
							                get_template_part( 'template-parts/content', 'cards' );
							            endwhile;
							            ?>
							            </div>
							            <div class="mx-auto">
							            	<?php
							            		the_posts_pagination( array( 'mid_size' => 2 ) );						   
							        else :
							            get_template_part( 'template-parts/content', 'none' );
					        		endif; 
					    			?>
										</div>
							</div>
				        </div>
				    </div>
				</main><!-- #main -->
			</section><!-- #primary -->

		</div><!-- .row -->
	</div><!-- .container -->
</div>
<?php
get_footer();
