<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<section  id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<?php
			the_content();

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'wp-bootstrap-starter' ),
				'after'  => '</div>',
			) );
		?>
	

	<div class="page-content container col-8">

	<?php
	    $categories = get_categories(); 

	    foreach ( $categories as $category ) {
	    
	      $args = array(
	          'cat' => $category->term_id,
	          'post_type' => 'post',
	          'posts_per_page' => '4',
	      );

	      $query = new WP_Query( $args );
	      
	      if ( $query->have_posts() ) { ?>
	          <?php
	              $category_id = get_cat_ID( $category->name );
	              $category_link = get_category_link( $category_id );
	          ?>
	          <div class="listing row news-block">
	            
	            <div class="col-12">
	              <div class="categories row justify-content-between">
	                <div><h2>Latest in <span><?php echo $category->name; ?></span></h2></div>
	                <div class="pt-1"><a href="<?php echo esc_url( $category_link ); ?>" title="See all in <?php echo $category->name; ?>">See all<i class="fas fa-angle-right"></i></a></div>
	              </div>
	            </div>
	            	
	            <div class="card-deck">
	              
	              <?php while ( $query->have_posts() ) {
	      
	                  $query->the_post();
	                  ?>
						  <div class="card">
						  	<div class="card-img">
							  	<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_post_thumbnail('sml-img', array( 'class' => 'card-img-top' )) ?></a>
							 </div>
						    <div class="card-body">
						    	<div class="category"><?php the_category( ', ' ); ?></div>
						      	<h5 class="card-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
						    </div>
						    <div class="card-time"><small class="text-muted"><?php the_time('m d Y'); ?></small></div>
						  </div>
	      
	              <?php } // end while ?>
	      			</div>
	          </div>
	      
	      <?php } // end if
	      
	      // Use reset to restore original query.
	      wp_reset_postdata();

	    }
	    ?>


		</div><!-- .page-content -->
	</div><!-- .entry-content -->
</section><!-- .no-results -->

