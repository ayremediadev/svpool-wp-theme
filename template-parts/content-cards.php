<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

?>

<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12  mb-4">

	<div class="card h-100  mx-0">
		  	<div class="card-img">
			  	<a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_post_thumbnail('sml-img', array( 'class' => 'card-img-top' )) ?></a>
			 </div>
		    <div class="card-body">
		    	<div class="category"><?php the_category( ', ' ); ?></div>
		      	<h5 class="card-title"><a href="<?php the_permalink(); ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h5>
		    </div>
		    <div class="card-time"><small class="text-muted"><?php the_time('m d Y'); ?></small></div>
	</div>

</div>

