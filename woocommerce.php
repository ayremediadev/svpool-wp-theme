<?php
/**
 * The template for displaying Woocommerce Product
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>



<div id="content" class="site-content page">
	<div class="container-fluid">

		<div class="row">

			<section id="primary" class="content-area col-sm-8 mx-auto my-5">
				<main id="main" class="site-main" role="main">

	            <?php woocommerce_content(); ?>

	        </main><!-- #main -->
	    </section><!-- #primary -->

	</div><!-- .row -->
</div><!-- .container -->

</div><!-- #content -->
<?php
get_footer();
