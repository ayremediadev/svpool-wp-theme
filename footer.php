<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>

    <?php get_template_part( 'footer-widget' ); ?>

    <div class="footer-border">
    	<div class="right"></div>
	    <div class="left"></div>
	</div>

	<footer id="colophon" class="site-footer <?php echo wp_bootstrap_starter_bg_class(); ?>" role="contentinfo">
		<div class="container pt-5 pb-4">
			<div class="row" style="">

				<div class="col-lg-2 col-md-12 logo" id="a">
					<a href="<?php echo esc_url( home_url( '/' )); ?>">
                        <img src="<?php bloginfo('template_directory'); ?>/inc/assets/img/logo.svg" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                    </a>
	            </div>

	            <div class="col-lg-8 col-md-12" id="b">
	            	<hr class="divider">
	            	<nav class="navbar navbar-expand-lg p-0">
	            	<?php
	                    wp_nav_menu(array(
	                    'theme_location'  => 'footer',
	                    'container'       => 'div',
	                    'container_id'    => 'footer-nav',
	                    'container_class' => 'navbar-collapse justify-content-around',
	                    'menu_id'         => false,
	                    'menu_class'      => 'navbar-nav',
	                    'depth'           => 3,
	                    'fallback_cb'     => 'wp_bootstrap_navwalker::fallback',
	                    'walker'          => new wp_bootstrap_navwalker()
	                    ));
	                    ?>
	                </nav>
	                <hr class="divider">
	            </div>

	            <div class="col-lg-2 col-md-12 lang" id="c">

	            	<?php do_action('icl_language_selector'); ?>
	            	
	            	<!-- <nav class="navbar navbar-expand-lg p-0">
	            	<ul class="navbar-nav dropup">
					    <li class="nav-item dropdown">
					      <a class="nav-item nav-link dropdown-toggle" href="#" id="bd-versions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
					        CN
					      </a>
					      <div class="dropdown-menu dropdown-menu-right" aria-labelledby="bd-versions">
					        <a class="dropdown-item active" href="">EN</a>
					        <a class="dropdown-item" href="">CN</a>
					      </div>
					    </li>  
					  </ul>
					</nav> -->
	            </div>

            </div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>
</div><!-- #page -->

<?php wp_footer(); ?>
</body>
</html>