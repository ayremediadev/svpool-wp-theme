<?php
/**
Listing of posts
* Template Name: Template - List
 */

get_header(); ?>

<div id="content" class="site-content page">    

	<div class="container-fluid">
		<div class="row">	

			<section id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

					 
				<?php
					while ( have_posts() ) : the_post();

						get_template_part( 'template-parts/content', 'list' );

						// If comments are open or we have at least one comment, load up the comment template.
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;

					endwhile; // End of the loop.
					?>

				

				</main><!-- #main -->
			</section><!-- #primary -->

		</div><!-- .row -->
	</div><!-- .container -->

</div><!-- #content -->
<?php
get_footer();
