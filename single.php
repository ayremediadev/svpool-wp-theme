<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Bootstrap_Starter
 */

get_header(); ?>

<div id="content" class="site-content page">

	<div class="container-fluid">
		<div class="row">

			<section id="primary" class="content-area col-sm-12">
				<main id="main" class="site-main" role="main">

						<header class="entry-header post">

			            </header>
			            <div class="header-border">
			                <div class="top-left"></div>
			                <div class="top-right"></div>
			                <div class="bottom-left"></div>
			                <div class="bottom-right"></div>
			            </div>

			            <div class="container">
				            <div class="row">
								<div class="col-sm-12 col-lg-8">
									<?php
									while ( have_posts() ) : the_post();

										get_template_part( 'template-parts/content', get_post_format() );

										    // the_post_navigation();

										// if ( comments_open() || get_comments_number() ) :
										// 	comments_template();
										// endif;

									endwhile; // End of the loop.
									?>
								</div>

								<?php
								get_sidebar();
								?>
							</div>
						</div>

				</main><!-- #main -->
			</section><!-- #primary -->

		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- #content -->
<?php
get_footer();
